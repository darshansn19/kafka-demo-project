package org.training.kafkaspringbootexample.dto;

import lombok.Data;

@Data
public class ProducerDto {
	
	private String message;

}
