package org.training.kafkaspringbootexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaspringbootexampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaspringbootexampleApplication.class, args);
	}

}
