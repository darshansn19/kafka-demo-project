package org.training.kafkaspringbootexample.service;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class KafkaProducerService {

	private  KafkaTemplate<String, String> kafkaTemplate;


	public void sendMessage(String message) {
		log.info(String.format("Message sent -> %s", message));
		kafkaTemplate.send("test-message",message);
	}
}

