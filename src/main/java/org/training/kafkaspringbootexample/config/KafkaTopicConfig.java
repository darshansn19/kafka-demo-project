package org.training.kafkaspringbootexample.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

	@Bean
	public NewTopic createTopic() {
		return new NewTopic("test-message", 2, (short) 1);

	}
    
//    @Bean
//    NewTopic createTopic()
//	{
//		return TopicBuilder.name("test-message").build();
//		
//	}
}
